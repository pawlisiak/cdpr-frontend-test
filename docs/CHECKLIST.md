# Recruitment task checklist 

## Technical requirements:
### Use package manager of your choice (NPM/Yarn).

I used `npm`, anyway I have experience with bioth of them and like them the same 🤷‍♂️

### Use latest Electron version.
Used `electron@21.1.1`.

### Use latest TypeScript with correct typings ("any" type is dissallowed unless strictly necessary).
Used `typescript@4.8.4`. Typings are set properly.

### Use latest React (please use function components and hooks in your code).
Used `react@18.2.0`.

### Use latest Webpack with configured tree shaking.
Used `webpack@5.74.0`. Tree shaking is configured in `@nrwl/webpack:webpack` executor used to build `ui` application, anyway I overwritten webpack config in `./apps/ui/custom-webpack.config.js`. 

### Use latest ESLint with rule set of your choice. Use latest Prettier. Configure both ESLint and Prettier to auto-format code on file save in VS Code.
`eslint@8.25.0` added. Nx is using specific ESLint configs for project anyway I customized it a bit. Prettier auto-formatting configured in IDE.

### Use stylesheet management system of your choice (for example Emotion CSS, SASS or even plain CSS is allowed).
Used SCSS.

### In package.json provide two scripts: dev (launching the app with file watcher and hot reload) and dist (packing the app for the distribution). The distributed package doesn't have to be an installer, it can be a directory with loose files. Packaged distribution should target Windows.
`npm run build` for building executables and `npm start` for serving the project. Also `npm run dev` what is alias for `start` script. Unfortunatelly I tried `nx-electron` executor for NX for the first time and didn't figure it out how to build Windows app on MacOS computer, but it will build Windows application on Windows and MacOS application on MacOS, correctly.

### You can use open-source UI frameworks of your choice, but you can also prepare the UI from scratch.
I am psychofan of creating own UI libraries/Design Systems, so designed and created my own (including icons) from scratch. It supports passing design tokens as CSS variables, supports dark mode and can be reusable in other projects.

## User scenarios:
### I want the title bar to be consistent with the rest of UI, so it cannot be Electron's default one.

I used `frame: false` option for `BrowserWindow` and created title bar from scratch.

### I want to navigate backwards/forwards using buttons.

Application supports Windows, Linux (basically the same) and MacOS keyboard shortcuts.

### I want to minimize/maximize/close the application using buttons on title bar.

Buttons are working. Application also supports Windows, Linux (basically the same) and MacOS keyboard shortcuts.

### I want to have a two-panel view. Left panel should contain a static list of common locations (Desktop / Downloads / Documents).

Left panel gets location and names of the folders using Electron's `getPath` method. Updated every 5sec.

### I want to see the list of folders and files in the right panel. Each item should have an icon indicating if it's a folder or a file.

Icons are present. Additionally added very simple extension caption for files' icons.

### I want to double-click a folder on right panel to navigate to its contents.

Double click opens folder in the app.

### I want to double-click a file on right panel to open it in the default program (for example text files in Notepad etc).

Double click executes the file in default OS's application.

### I want the list of files on right panel to refresh automatically in the background with 5 second interval. For example, when I delete a file from Downloads folder (using Windows Explorer), I want the app to reflect that change.

Content are refreshed imedietely on current location change, and in 5sec interval. Refreshes are asynchronous.

## Special features

To see special features I implemented to the app take a look at [FEATURES.md](./FEATURES.md).