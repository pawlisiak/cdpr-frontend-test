# RED File Browser features

## OS theme support
It will change the window styling imediatelly on theme change.

![OS theme support](./assets/themes.png)

## Responsive design
Sidebar and action buttons hide when window is smaller. Minimum width and height of the window is set.

![Responsive design](./assets/responsive-design.gif)

## Accessibility
Application can be navigated by keyboard. Focusable elements can be read by screen reader.

## Toolbars
Move cursor over button to see OS tooltip with action label and keyboard shortcut.

## Keyboard shortcuts for different OS
Application detects which OS is in use and customize shortcuts for them.

## Language detector and Polish support
Application detects OS language and can show Polish Labels. Default language is English.

## Open current folder in OS native browser
Just click on "Open in Finder/Explorer" button in sidebar.