

# CDPR - Frontend Test

This is a project made for CDPR Frontend Developer with React (DevOps) recruitment purposes. This file tells only about building and executing the project. Take a look at this files:

* [Features of the application](./docs/FEATURES.md)
* [Task checklist](./docs/CHECKLIST.md)

## Prerequisities

The project was built using [Node v16.17.1](https://nodejs.org/download/release/v16.17.1/) and [NPM v8.15.0](https://nodejs.org/download/release/v16.17.1/). It's safe to use same or newer versions, so if oyu'd have any issues please update the tooling.

Tool used to generate the workspace is NX. It's not necessary to install the package globally, but it will give you some usefool tools. More about NX [here](./docs/NX.md).

### Dependencies instalation

To install all node depencencies, simply run:

```bash
npm ci
```

_Note: There is some kind of issue with `nx-electron` package `postinstall` script, that's why this package is installed on `postinstall` stage:_

```json
{
  "scripts": {
    // ...
    "postinstall": "npm i nx-electron@13.2.1 --no-save"
  }
}
```

## Project structure

Codebase is splitted between applications and libraries. Applications are mostly buildable or servable part of code, libraries are local dependencies or parts common for several appliactions.

| Project name | Path | Descrition |
| ------------ | ---- | ---------- |
| ui | `apps/ui` | Main React application containing all application logic and state. It's included by electron app |
| ui-e2e | `apps/ui-e2e` | Cypress E2E tests (Not configured) |
| electron | `apps/electron` | Electron application config used to serve and make an application |
| design-system | `libs/design-system` | Library of common components (can be used in other applications) |
| locales | `libs/locales` | Localization for React app and components |

![Project graph](/docs/assets/graph.png)

## Serving project

To serve the project in development mode, run:

```bash
npm start
```

It will serve `electron` and `ui` projects and open the application. You have access to DevTools in this mode.

_Note: React app is starting a bit slower (~5sec) in developent mode so Electron window will be empty on init. It won't be visible after build._

For better processes customization you can use `nx` commands e.g. to clean caches, start/stop specific project, etc.

## Building executables

To build executables, run:

```bash
npm run build
```

It will build `ui` and `electron` projects, then build executable using `electron-build` tool.
Application will be built in [dist/executables](/dist/executables).