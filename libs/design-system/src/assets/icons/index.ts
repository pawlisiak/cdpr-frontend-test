import ArrowLeft from './arrow-left.svg';
import ArrowRight from './arrow-right.svg';
import Close from './close.svg';
import Directory from './directory.svg';
import File from './file.svg';
import GoUp from './go-up.svg';
import WindowMaximize from './window-maximize.svg';
import WindowMinimize from './window-minimize.svg';

export interface IconsModel {
  [key: string]: string;
}

export const icons: IconsModel = {
  'arrow-left': ArrowLeft,
  'arrow-right': ArrowRight,
  close: Close,
  directory: Directory,
  file: File,
  'go-up': GoUp,
  'window-maximize': WindowMaximize,
  'window-minimize': WindowMinimize,
};
