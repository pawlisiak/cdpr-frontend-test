import React, { FC, ReactNode } from 'react';
import classNames from 'classnames';
import styles from './MainView.module.scss';

export interface MainViewProps {
  sidebar?: ReactNode;
  content: ReactNode;
  titlebar: ReactNode;
}

export const MainView: FC<MainViewProps> = ({ sidebar, titlebar, content }) => {
  const mainViewStyles = classNames(styles['main-view']);

  return (
    <div className={mainViewStyles}>
      {sidebar}
      {titlebar}
      {content}
    </div>
  );
};
