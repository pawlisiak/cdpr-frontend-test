import { FC, KeyboardEvent, useCallback, useMemo, useState } from 'react';
import { Icon } from '../..';
import classNames from 'classnames';
import styles from './File.module.scss';

export interface FileProps {
  fileName: string;
  fileType: 'directory' | 'file' | 'unknown';
  isHidden?: boolean;
  onDoubleClick?: () => void;
  showExtension?: boolean;
  title?: string;
}

export const File: FC<FileProps> = ({
  fileName,
  fileType = 'unknown',
  isHidden = false,
  showExtension = false,
  onDoubleClick = () => {
    return;
  },
}) => {
  const [isFocused, setIsFocused] = useState(false);

  const fileClasses = classNames(
    styles['file'],
    isHidden && styles['is-hidden'],
    isFocused && styles['is-focused']
  );

  const fileExtension = useMemo(() => {
    if (fileType !== 'file') {
      return null;
    }

    const lastPart = fileName.split('.').pop();

    if (!lastPart || lastPart.length > 5) {
      return null;
    }

    return lastPart;
  }, [fileType, fileName]);

  const handleFocus = useCallback(() => {
    setIsFocused(true);
  }, [setIsFocused]);

  const handleBlur = useCallback(() => {
    setIsFocused(false);
  }, [setIsFocused]);

  const handleKeyDown = (ev: KeyboardEvent<HTMLButtonElement>) => {
    const { key } = ev;

    if (key === 'Enter' || key === ' ') {
      onDoubleClick();
      ev.preventDefault();
    }
  };

  return (
    <button
      onFocus={handleFocus}
      onBlur={handleBlur}
      onKeyDown={handleKeyDown}
      onDoubleClick={onDoubleClick}
      className={fileClasses}
      title={`${fileName} (${fileType})`}
    >
      <figure className={classNames(styles['icon'])}>
        <Icon name={fileType} size={64} />

        {showExtension && fileType === 'file' && fileExtension && (
          <figcaption className={classNames(styles['extension'])}>
            {fileExtension}
          </figcaption>
        )}
      </figure>

      <span className={classNames(styles['name'])}>{fileName}</span>
    </button>
  );
};
