import React, { FC, HTMLAttributes, ReactNode } from 'react';
import classNames from 'classnames';
import styles from './Button.module.scss';

export interface ButtonProps extends HTMLAttributes<HTMLButtonElement> {
  children?: ReactNode;
  className?: string;
  isDisabled?: boolean;
  isReadonly?: boolean;
  size?: 'small' | 'regular';
  styleType?: 'primary' | 'secondary' | 'ghost';
}

export const Button: FC<ButtonProps> = ({
  children,
  className,
  isDisabled = false,
  isReadonly = false,
  size = 'regular',
  styleType = 'primary',
  ...rest
}) => {
  const buttonStyles = classNames(
    styles['button'],
    styleType && styles[styleType],
    size && styles[size],
    isReadonly && styles['is-readonly'],
    isDisabled && styles['is-disabled'],
    className
  );

  return (
    <button
      className={buttonStyles}
      disabled={isDisabled || isReadonly}
      {...rest}
    >
      {children}
    </button>
  );
};
