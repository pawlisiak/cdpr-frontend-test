import { FC, HTMLAttributes, ReactNode } from 'react';
import classNames from 'classnames';
import styles from './ButtonGroup.module.scss';

export interface ButtonGroupProps extends HTMLAttributes<HTMLDivElement> {
  children?: ReactNode;
  className?: string;
  layout?: 'horizontal' | 'vertical';
}

export const ButtonGroup: FC<ButtonGroupProps> = ({
  children,
  className,
  layout = 'horizontal',
  ...rest
}) => {
  const buttonGroupClasses = classNames(
    styles['button-group'],
    styles[layout],
    className && className
  );

  return (
    <div className={buttonGroupClasses} {...rest}>
      {children}
    </div>
  );
};
