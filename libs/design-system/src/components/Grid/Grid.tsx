import React, {
  CSSProperties,
  FC,
  HTMLAttributes,
  ReactNode,
  useMemo,
} from 'react';
import classNames from 'classnames';
import styles from './Grid.module.scss';

export interface GridProps extends HTMLAttributes<HTMLDivElement> {
  size?: number;
  children?: ReactNode;
  style?: CSSProperties;
}

export const Grid: FC<GridProps> = ({
  size = 96,
  children,
  style,
  ...rest
}) => {
  const gridClasses = classNames(styles['grid']);

  const gridStyles = useMemo(
    () => ({
      gridTemplateColumns: `repeat(auto-fill, minmax(${size}px, 1fr))`,
      style,
    }),
    [size, style]
  );

  return (
    <div className={gridClasses} style={gridStyles}>
      {children}
    </div>
  );
};
