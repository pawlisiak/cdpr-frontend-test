import React, { FC, ReactNode } from 'react';
import classNames from 'classnames';
import styles from './Sidebar.module.scss';

export interface SidebarProps {
  children?: ReactNode;
  defaultWidth?: number;
}

export const Sidebar: FC<SidebarProps> = ({ children, defaultWidth = 192 }) => {
  const sidebarStyles = classNames(styles['sidebar']);

  return (
    <aside className={sidebarStyles} style={{ width: `${defaultWidth}px` }}>
      <figure className={classNames(styles['dragbar'])}></figure>

      {children}
    </aside>
  );
};
