import { render, RenderResult } from '@testing-library/react';

import { Sidebar } from './Sidebar';

const renderComponent = (): RenderResult => render(<Sidebar></Sidebar>);

describe('Sidebar', () => {
  it('should render successfully', () => {
    const { baseElement } = renderComponent();
    expect(baseElement).toMatchSnapshot();
  });

  it('should contain children if provided', () => {
    const { baseElement } = render(
      <Sidebar>
        <span>content</span>
      </Sidebar>
    );

    const children = baseElement.querySelector('span');

    expect(children).toBeTruthy();
  });
});
