import React, { FC, ReactNode } from 'react';
import classNames from 'classnames';
import styles from './Content.module.scss';

export interface ContentProps {
  children?: ReactNode;
}

export const Content: FC<ContentProps> = ({ children }) => {
  return <div className={classNames(styles['content'])}>{children}</div>;
};
