import { FC, SVGProps, useMemo } from 'react';
import { icons } from '../../assets/icons';
import classNames from 'classnames';
import styles from './Icon.module.scss';

export interface IconProps extends SVGProps<SVGSVGElement> {
  name: string;
  size?: number;
}

export const Icon: FC<IconProps> = ({ name, size = 96, ...rest }) => {
  const IconSVG = useMemo(() => icons[name], [name]);

  return (
    <IconSVG
      className={classNames(styles['icon'])}
      width={size}
      height={size}
      {...rest}
    />
  );
};
