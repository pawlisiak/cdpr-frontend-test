import {
  render,
  screen,
  fireEvent,
  RenderResult,
} from '@testing-library/react';

import { Titlebar } from './Titlebar';

const renderComponent = (): RenderResult => render(<Titlebar></Titlebar>);

describe('Titlebar', () => {
  beforeEach(() => {
    window.electron = {
      closeWindow: jest.fn(),
      maximizeWindow: jest.fn(),
      minimizeWindow: jest.fn(),
      isWindows: false,
      isMac: true,
      rootDir: '/',
    };
  });

  it('should render successfully', () => {
    const { baseElement } = render(<Titlebar></Titlebar>);
    expect(baseElement).toMatchSnapshot();
  });

  it('should show action and labl if are passed', () => {
    const { baseElement } = render(
      <Titlebar actions={<span>actions</span>} label="label"></Titlebar>
    );

    const actionsSlot = baseElement.querySelector('.app-actions')?.textContent;
    const labelSlot = baseElement.querySelector('.title')?.textContent;

    expect(actionsSlot).toContain('actions');
    expect(labelSlot).toContain('label');
  });

  it('should show MacOS keyboard shortcuts if run in MacOS', () => {
    renderComponent();

    const closeButton = screen.getByTitle('close', { exact: false });
    const minimizeButton = screen.getByTitle('minimize', { exact: false });
    const maximizeButton = screen.getByTitle('maximize', { exact: false });

    expect(closeButton.title).toContain('⌘W');
    expect(minimizeButton.title).toContain('⌘M');
    expect(maximizeButton.title).toContain('⌃⌥↩︎');
  });

  it('should show MacOS keyboard shortcuts if run in MacOS', () => {
    window.electron.isWindows = true;
    window.electron.isMac = false;

    renderComponent();

    const closeButton = screen.getByTitle('close', { exact: false });
    const minimizeButton = screen.getByTitle('minimize', { exact: false });
    const maximizeButton = screen.getByTitle('maximize', { exact: false });

    expect(closeButton.title).toContain('Alt + F4');
    expect(minimizeButton.title).toContain('Win + ↓');
    expect(maximizeButton.title).toContain('Win + ↑');
  });

  it('click on Close button should launch closeWindow() method', () => {
    renderComponent();

    fireEvent.click(screen.getByTitle('close', { exact: false }));

    expect(window.electron.closeWindow).toHaveBeenCalled();
  });

  it('click on Minimize button should launch minimizeWindow() method', () => {
    renderComponent();

    fireEvent.click(screen.getByTitle('minimize', { exact: false }));

    expect(window.electron.minimizeWindow).toHaveBeenCalled();
  });

  it('click on Maximize button should launch maximizeWindow() method', () => {
    renderComponent();

    fireEvent.click(screen.getByTitle('maximize', { exact: false }));

    expect(window.electron.maximizeWindow).toHaveBeenCalled();
  });
});
