import React, { FC, useCallback, useEffect, useState, ReactNode } from 'react';
import { Button, ButtonGroup, Icon } from '../..';
import i18next from '@cdpr/locales';
import classNames from 'classnames';
import styles from './Titlebar.module.scss';

export interface TitlebarProps {
  actions?: ReactNode;
  label?: string | ReactNode;
  labelPosition?: 'left' | 'center' | 'right';
}

export const Titlebar: FC<TitlebarProps> = ({
  actions,
  label,
  labelPosition = 'center',
}) => {
  const { closeWindow, maximizeWindow, minimizeWindow } = window.electron;
  const { t } = i18next;
  const { isMac } = window.electron;

  const [isFocused, setIsFocused] = useState(true);

  const focusWindow = useCallback(() => {
    setIsFocused(true);
  }, [setIsFocused]);

  const blurWindow = useCallback(() => {
    setIsFocused(false);
  }, [setIsFocused]);

  useEffect(() => {
    window.addEventListener('focus', focusWindow);
    return () => {
      window.removeEventListener('focus', focusWindow);
    };
  }, [focusWindow]);

  useEffect(() => {
    window.addEventListener('blur', blurWindow);
    return () => {
      window.removeEventListener('blur', blurWindow);
    };
  }, [blurWindow]);

  const titlebarStyles = classNames(
    styles['titlebar'],
    isFocused && styles['is-focused'],
    labelPosition === 'left' && styles['left-aligned'],
    labelPosition === 'right' && styles['right-aligned']
  );

  return (
    <header className={titlebarStyles}>
      <div className={classNames(styles['app-actions'])}>{actions}</div>

      <h1 className={classNames(styles['title'])}>{label}</h1>

      <ButtonGroup className={classNames(styles['window-actions'])}>
        <Button
          onClick={minimizeWindow}
          styleType="secondary"
          tabIndex={-1}
          title={t('window.minimize', { shortcut: isMac ? '⌘M' : 'Win + ↓' })}
        >
          <Icon size={24} name="window-minimize" />
        </Button>

        <Button
          onClick={maximizeWindow}
          styleType="secondary"
          tabIndex={-1}
          title={t('window.maximize', { shortcut: isMac ? '⌃⌥↩︎' : 'Win + ↑' })}
        >
          <Icon size={24} name="window-maximize" />
        </Button>

        <Button
          onClick={closeWindow}
          styleType="secondary"
          tabIndex={-1}
          title={t('window.close', { shortcut: isMac ? '⌘W' : 'Alt + F4' })}
        >
          <Icon size={24} name="close" />
        </Button>
      </ButtonGroup>
    </header>
  );
};
