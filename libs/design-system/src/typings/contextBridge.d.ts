export declare global {
  interface ContentModel {
    name: string;
    type: 'directory' | 'file' | 'unknwown';
    path: string;
  }

  interface Window {
    api: {
      bookmarksContents: () => Promise<Array<ContentModel>>;
      currentDirectory: () => string;
      directoryContents: (path: string) => Promise<Array<ContentModel>>;
      openFile: (path: string) => void;
    };
    electron: {
      closeWindow: () => void;
      maximizeWindow: () => void;
      minimizeWindow: () => void;
      isWindows: boolean;
      isMac: boolean;
      rootDir: string;
    };
  }
}
