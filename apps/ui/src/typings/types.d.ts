interface FileModel {
  name: string;
  type: 'directory' | 'file' | 'unknown';
  path: string;
}
