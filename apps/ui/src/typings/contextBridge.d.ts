export declare global {
  interface Window {
    api: {
      bookmarksContents: () => Promise<Array<FileModel>>;
      currentDirectory: () => string;
      directoryContents: (path: string) => Promise<Array<FileModel>>;
      openFile: (path: string) => void;
    };
    electron: {
      closeWindow: () => void;
      maximizeWindow: () => void;
      minimizeWindow: () => void;
      isWindows: boolean;
      isMac: boolean;
      rootDir: string;
    };
  }
}
