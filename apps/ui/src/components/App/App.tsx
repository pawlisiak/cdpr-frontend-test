import { FC, useState } from 'react';
import { RouteContext } from '../../contexts/RouteContext';
import { AppTitlebar } from '../AppTitlebar/AppTitlebar';
import { AppSidebar } from '../AppSidebar/AppSidebar';
import { FileList } from '../FileList/FileList';
import { MainView } from '@cdpr/design-system';

export const App: FC = () => {
  const { currentDirectory } = window.api;
  const [currentRoute, setCurrentRoute] = useState(currentDirectory());
  const [previousRoutes, setPreviousRoutes] = useState<Array<string>>([]);
  const [nextRoutes, setNextRoutes] = useState<Array<string>>([]);

  return (
    <RouteContext.Provider
      value={{
        currentRoute,
        previousRoutes,
        nextRoutes,
        setCurrentRoute,
        setPreviousRoutes,
        setNextRoutes,
      }}
    >
      <MainView
        sidebar={<AppSidebar />}
        titlebar={<AppTitlebar />}
        content={<FileList />}
      />
    </RouteContext.Provider>
  );
};

export default App;
