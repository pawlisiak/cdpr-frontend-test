import React, {
  FC,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react';
import { setIntervalAsync, clearIntervalAsync } from 'set-interval-async';
import { navigateTo } from '../../helpers/navigate';
import { rootDir } from '../../helpers/pathHelpers';
import { RouteContext } from '../../contexts/RouteContext';
import { Content, File, Grid } from '@cdpr/design-system';

export const FileList: FC = () => {
  const [previousRoute, setPreviousRoute] = useState<string>();
  const [directoryContents, setDirectoryContents] = useState<Array<FileModel>>(
    []
  );

  const routeContext = useContext(RouteContext);
  const { currentRoute, setCurrentRoute, setNextRoutes, setPreviousRoutes } =
    routeContext;

  const sortedFiles = useMemo(
    () =>
      directoryContents &&
      [...directoryContents].sort((a, b) => (a.type < b.type ? -1 : 1)),
    [directoryContents]
  );

  const fetchDirectoryContents = useCallback(
    async (path: string) => {
      try {
        const response = await window.api.directoryContents(currentRoute);
        setDirectoryContents(response);
      } catch (err) {
        setCurrentRoute(rootDir);
        setPreviousRoutes([]);
        setNextRoutes([]);
      }
    },
    [
      currentRoute,
      setCurrentRoute,
      setDirectoryContents,
      setPreviousRoutes,
      setNextRoutes,
    ]
  );

  useEffect(() => {
    const filesFetchingTimer = setIntervalAsync(() => {
      fetchDirectoryContents(currentRoute);
    }, 5000);
    return () => {
      clearIntervalAsync(filesFetchingTimer);
    };
  });

  if (previousRoute !== currentRoute) {
    fetchDirectoryContents(currentRoute);
    setPreviousRoute(currentRoute);
  }

  const handleDoubleClick = (entry: FileModel): void => {
    const { openFile } = window.api;

    if (entry.type === 'directory') {
      navigateTo(entry.path, routeContext);
      return;
    }

    openFile(`${entry.path}`);
  };

  return (
    <Content>
      <Grid style={{ marginRight: '1em' }}>
        {sortedFiles &&
          sortedFiles.map((entry: FileModel, i: number) => {
            return (
              <File
                fileName={entry.name}
                fileType={entry.type}
                showExtension={true}
                key={i}
                onDoubleClick={() => handleDoubleClick(entry)}
              />
            );
          })}
      </Grid>
    </Content>
  );
};
