import React, { FC, useContext } from 'react';
import { RouteContext } from '../../contexts/RouteContext';
import { slash } from '../../helpers/pathHelpers';
import { AppTitlebarActions } from '../AppTitlebarActions/AppTitlebarActions';
import { Titlebar } from '@cdpr/design-system';

export const AppTitlebar: FC = () => {
  const { currentRoute } = useContext(RouteContext);
  const windowLabel = currentRoute.split(slash).pop();

  return <Titlebar actions={<AppTitlebarActions />} label={windowLabel} />;
};
