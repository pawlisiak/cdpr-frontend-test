import React, { FC, useContext, useMemo } from 'react';
import { RouteContext } from '../../contexts/RouteContext';
import { useTranslation } from 'react-i18next';
import { Button } from '@cdpr/design-system';
import classNames from 'classnames';
import styles from './DirectoryAction.module.scss';

export const DirectoryAction: FC = () => {
  const { t } = useTranslation();
  const { isMac } = window.electron;
  const { openFile } = window.api;

  const { currentRoute } = useContext(RouteContext);

  const browserApp = useMemo(() => (isMac ? 'Finder' : 'Explorer'), [isMac]);

  return (
    <section className={classNames(styles['directory-action'])}>
      <Button
        styleType="secondary"
        className={classNames(styles['button'])}
        onClick={() => openFile(currentRoute)}
        title={t('openDirectory', { app: browserApp })}
      >
        {t('openDirectory', { app: browserApp })}
      </Button>
    </section>
  );
};
