import React, { FC } from 'react';
import { Bookmarks } from '../Bookmarks/Bookmarks';
import { DirectoryAction } from '../DirectoryAction/DirectoryAction';
import { Sidebar } from '@cdpr/design-system';

export const AppSidebar: FC = () => {
  return (
    <Sidebar>
      <Bookmarks />
      <DirectoryAction />
    </Sidebar>
  );
};
