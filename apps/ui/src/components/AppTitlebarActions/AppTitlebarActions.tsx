import React, { FC, useContext, useCallback, useMemo, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import {
  navigateBackward,
  navigateForward,
  navigateUp,
} from '../../helpers/navigate';
import { slash, rootDir } from '../../helpers/pathHelpers';
import { RouteContext } from '../../contexts/RouteContext';
import { Button, Icon } from '@cdpr/design-system';

export const AppTitlebarActions: FC = () => {
  const { isWindows, isMac } = window.electron;
  const { t } = useTranslation();

  const routeContext = useContext(RouteContext);
  const { currentRoute, previousRoutes, nextRoutes } = routeContext;

  const upRoute = useMemo(
    () =>
      currentRoute === rootDir
        ? null
        : currentRoute.substring(0, currentRoute.lastIndexOf(slash)) || rootDir,
    [currentRoute]
  );

  const previousRoute = useMemo(
    () => previousRoutes[0] || null,
    [previousRoutes]
  );

  const nextRoute = useMemo(() => nextRoutes[0] || null, [nextRoutes]);

  const handleUserKeyDown = useCallback(
    (ev: KeyboardEvent) => {
      const { altKey, key, metaKey } = ev;

      if (
        (!isMac &&
          ((isWindows && key === 'Backspace') ||
            (altKey && key === 'ArrowLeft'))) ||
        (isMac && metaKey && key === '[')
      ) {
        navigateBackward(previousRoute, routeContext);
        ev.preventDefault();
        return;
      }

      if (
        (!isMac && altKey && key === 'ArrowRight') ||
        (isMac && metaKey && key === ']')
      ) {
        navigateForward(nextRoute, routeContext);
        ev.preventDefault();
        return;
      }

      if (
        (!isMac && altKey && key === 'ArrowUp') ||
        (isMac && metaKey && key === 'ArrowUp')
      ) {
        navigateUp(upRoute, routeContext);
        ev.preventDefault();
        return;
      }
    },
    [isWindows, isMac, routeContext, previousRoute, nextRoute, upRoute]
  );

  useEffect(() => {
    document.addEventListener('keydown', handleUserKeyDown);
    return () => {
      document.removeEventListener('keydown', handleUserKeyDown);
    };
  }, [handleUserKeyDown]);

  return (
    <>
      <Button
        isDisabled={!previousRoute}
        onClick={() => navigateBackward(previousRoute, routeContext)}
        styleType="ghost"
        title={t('navigation.goBackward', {
          shortcut: isMac ? '⌘[' : 'Alt + ←',
        })}
      >
        <Icon size={24} name="arrow-left" />
      </Button>

      <Button
        isDisabled={!nextRoute}
        onClick={() => navigateForward(nextRoute, routeContext)}
        styleType="ghost"
        title={t('navigation.goForward', {
          shortcut: isMac ? '⌘]' : 'Alt + →',
        })}
      >
        <Icon size={24} name="arrow-right" />
      </Button>

      <Button
        isDisabled={!upRoute}
        onClick={() => navigateUp(upRoute, routeContext)}
        styleType="ghost"
        title={t('navigation.goUp', {
          shortcut: isMac ? '⌘↑' : 'Alt + ↑',
        })}
      >
        <Icon size={24} name="go-up" />
      </Button>
    </>
  );
};
