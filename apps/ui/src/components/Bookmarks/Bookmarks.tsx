import React, { FC, useCallback, useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { setIntervalAsync, clearIntervalAsync } from 'set-interval-async';
import { RouteContext } from '../../contexts/RouteContext';
import { Button, Icon } from '@cdpr/design-system';
import { navigateTo } from '../../helpers/navigate';
import classNames from 'classnames';
import styles from './Bookmarks.module.scss';

export const Bookmarks: FC = () => {
  const { t } = useTranslation();

  const [bookmarksContents, setBookmarksContents] = useState<Array<FileModel>>(
    []
  );

  const routeContext = useContext(RouteContext);
  const { currentRoute } = routeContext;

  const fetchBookmarksContents = useCallback(async () => {
    const response = await window.api.bookmarksContents();
    setBookmarksContents(response);
  }, []);

  if (bookmarksContents.length === 0) {
    fetchBookmarksContents();
  }

  useEffect(() => {
    const filesFetchingTimer = setIntervalAsync(() => {
      fetchBookmarksContents();
    }, 5000);
    return () => {
      clearIntervalAsync(filesFetchingTimer);
    };
  });

  const isRouteActive = (path: string) => {
    return path === currentRoute;
  };

  return (
    <section className={classNames(styles['bookmarks'])}>
      <label className={classNames(styles['label'])}>{t('bookmarks')}</label>
      <nav className={classNames(styles['navigation'])}>
        {bookmarksContents &&
          bookmarksContents.map((entry, i) => (
            <Button
              key={i}
              onClick={() => navigateTo(entry.path, routeContext)}
              isReadonly={isRouteActive(entry.path)}
              styleType="ghost"
              className={classNames(
                styles['navigation-button'],
                isRouteActive(entry.path) && styles['is-active']
              )}
              title={entry.name}
            >
              <Icon name={entry.type} size={24} />

              <span>{entry.name}</span>
            </Button>
          ))}
      </nav>
    </section>
  );
};
