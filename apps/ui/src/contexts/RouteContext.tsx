import { createContext } from 'react';

export interface RouteContextModel {
  currentRoute: string;
  previousRoutes: Array<string>;
  nextRoutes: Array<string>;
  setCurrentRoute: (value: string) => void;
  setPreviousRoutes: (value: Array<string>) => void;
  setNextRoutes: (value: Array<string>) => void;
}

export const RouteContext = createContext({} as RouteContextModel);
