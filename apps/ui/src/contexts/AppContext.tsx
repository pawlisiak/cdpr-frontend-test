import { createContext } from 'react';

export interface AppContextModel {
  closeWindow?: () => void;
  isMac?: boolean;
  isWindows?: boolean;
  maximizeWindow?: () => void;
  minimizeWindow?: () => void;
}

const { closeWindow, isMac, isWindows, maximizeWindow, minimizeWindow } =
  window.electron;

export const AppContext = createContext({
  closeWindow,
  isMac,
  isWindows,
  maximizeWindow,
  minimizeWindow,
} as AppContextModel);
