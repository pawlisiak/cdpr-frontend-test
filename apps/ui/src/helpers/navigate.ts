import { RouteContextModel } from '../contexts/RouteContext';

export const navigateTo = (path: string, context: RouteContextModel): void => {
  const {
    currentRoute,
    setCurrentRoute,
    previousRoutes,
    setPreviousRoutes,
    setNextRoutes,
  } = context;

  setCurrentRoute(path);
  setPreviousRoutes([currentRoute, ...previousRoutes]);
  setNextRoutes([]);
};

export const navigateBackward = (
  previousRoute: string | null,
  context: RouteContextModel
) => {
  const {
    currentRoute,
    setCurrentRoute,
    previousRoutes,
    nextRoutes,
    setPreviousRoutes,
    setNextRoutes,
  } = context;

  if (!previousRoute) {
    return;
  }

  setNextRoutes([currentRoute, ...nextRoutes]);
  setCurrentRoute(previousRoute);
  setPreviousRoutes(previousRoutes.slice(1));
};

export const navigateForward = (
  nextRoute: string | null,
  context: RouteContextModel
) => {
  const {
    currentRoute,
    setCurrentRoute,
    previousRoutes,
    nextRoutes,
    setPreviousRoutes,
    setNextRoutes,
  } = context;

  if (!nextRoute) {
    return;
  }

  setCurrentRoute(nextRoute);
  setPreviousRoutes([currentRoute, ...previousRoutes]);
  setNextRoutes(nextRoutes.slice(1));
};

export const navigateUp = (
  upRoute: string | null,
  context: RouteContextModel
) => {
  const {
    currentRoute,
    setCurrentRoute,
    previousRoutes,
    setPreviousRoutes,
    setNextRoutes,
  } = context;

  if (!upRoute) {
    return;
  }

  setCurrentRoute(upRoute);
  setPreviousRoutes([currentRoute, ...previousRoutes]);
  setNextRoutes([]);
};
