const { isWindows, rootDir } = window.electron;

export const slash = isWindows ? '\\' : '/';

export { rootDir };
