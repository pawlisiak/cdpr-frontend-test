const { merge } = require('webpack-merge');

module.exports = (config, context) => {
  const webpackConfig = merge(config, {
    module: {
      rules: [
        {
          test: /\.svg$/i,
          use: [
            {
              loader: 'babel-loader',
            },
            {
              loader: 'react-svg-loader',
              options: {
                jsx: true,
              },
            },
          ],
        },
      ],
    },
    mode: 'production',
    optimization: {
      sideEffects: true,
    },
  });

  return webpackConfig;
};
