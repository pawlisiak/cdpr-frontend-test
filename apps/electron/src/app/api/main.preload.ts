import { contextBridge, ipcRenderer, shell } from 'electron';
import { readdir } from 'fs/promises';
import { basename, parse } from 'path';

const isWindows = process.platform === 'win32';

const directoryContents = async (path: string) => {
  const results = await readdir(path, { withFileTypes: true });

  return results.map((entry) => ({
    name: entry.name,
    path: `${path}${isWindows ? '\\' : '/'}${entry.name}`,
    type: entry.isDirectory() ? 'directory' : 'file',
  }));
};

const bookmarksContents = async () => {
  const desktopDirectoryPath = await ipcRenderer.invoke(
    'desktop-directory-path'
  );
  const documentsDirectoryPath = await ipcRenderer.invoke(
    'documents-directory-path'
  );
  const downloadsDirectoryPath = await ipcRenderer.invoke(
    'downloads-directory-path'
  );

  return [
    {
      name: basename(desktopDirectoryPath),
      path: desktopDirectoryPath,
      type: 'directory',
    },
    {
      name: basename(documentsDirectoryPath),
      path: documentsDirectoryPath,
      type: 'directory',
    },
    {
      name: basename(downloadsDirectoryPath),
      path: downloadsDirectoryPath,
      type: 'directory',
    },
  ];
};

const currentDirectory = (): string => {
  return process.cwd();
};

const openFile = (path: string): void => {
  shell.openPath(path);
};

contextBridge.exposeInMainWorld('api', {
  directoryContents,
  currentDirectory,
  openFile,
  bookmarksContents,
});

contextBridge.exposeInMainWorld('electron', {
  closeWindow: () => ipcRenderer.send('close-window'),
  maximizeWindow: () => ipcRenderer.send('maximize-window'),
  minimizeWindow: () => ipcRenderer.send('minimize-window'),
  isMac: process.platform === 'darwin',
  isWindows: isWindows,
  rootDir: parse(process.cwd()).root,
});
